; zundoko program

        .cr     6301            ; HD6301 Cross Overlay
        .lf     zundoko         ; List File Name
        .tf     zundoko.s19,s19

; ********************************************************************
; * Include Files                                                    *
; ********************************************************************
        .in     HD6303R_chip.def
        .in     service_routine.def

; ********************************************************************
; * 変数                                                             *
; ********************************************************************
        .sm     RAM
        .or     $80
zun_count       .bs     1       ; zunカウンタ
rnd_number      .bs     2       ; 乱数

; ********************************************************************
; * Program Start                                                    *
; ********************************************************************
        .sm     CODE
        .or     PROGRAM_START

set_seed:
        ldd     <FRC            ; Free run timer 読み出し
        bne     :1              ; Seedはゼロ以外
        inca
.1      std     <rnd_number

start:  ldx     #MSG_START
        jsr     putLine
        jsr     getChar

zundoko_check:
.top    clr     zun_count
.loop   bsr     xor_shift
        bmi     :doko
        ; 乱数が正数だったら
        ldx     #MSG_ZUN        ; print"zun"
        jsr     putLine
        inc     zun_count       ; zunカウンタ += 1
        bra     :loop
.doko   ; 乱数が負数だったら
        ldx     #MSG_DOKO       ; print"doko"
        jsr     putLine
        tim     #%11111100,<zun_count
        bne     :end
        ; zunカウンタ < 4
        bra     :top            ; zunカウンタ = 0
.end    ; zunカウンタ >= 4
        ldx     #MSG_KIYOSHI    ; print"kiyoshi"
        jsr     putLine
        bra     start

; 16-bit xorshift pseudorandom number generator
; http://www.retroprogramming.com/2017/07/xorshift-pseudorandom-numbers-in-z80.html
xor_shift:
        ldd     <rnd_number
        ; *** xs ^= xs << 7
        lsrd
        tba
        rorb
        andb    #$80
        eora    <rnd_number
        eorb    <rnd_number+1
        std     <rnd_number
        ; *** xs ^= xs >> 9
        tab
        clra
        lsrd
        eora    <rnd_number
        eorb    <rnd_number+1
        std     <rnd_number
        ; *** xs ^= xs << 8
        tba
        clrb
        eora    <rnd_number
        eorb    <rnd_number+1
        std     <rnd_number
        rts

MSG_START:      .az     "Hit any key.",#CR,#LF
MSG_ZUN:        .az     "zun "
MSG_DOKO:       .az     "doko "
MSG_KIYOSHI:    .az     "KI.YO.SHI!",#CR,#LF,#CR,#LF
