# SBC6303apps
Applications for the [SBC6303](https://vintagechips.wordpress.com/2018/04/26/sbc6303ルーズキット/).  
The SBC6303 is a single board computer operating with the Hitachi HD6303.  

## 概要
[SBC6303ルーズキット](https://vintagechips.wordpress.com/2018/04/26/sbc6303ルーズキット/)用のアプリケーションです。  
イニシャルプログラムローダー（IPL.asm）をROMに書き込んでください。  
SBC6303を起動し、プログラム（s19ファイル）を読み込ませてください。  
**$0000〜$1fffまでのRAM（8Kバイト）が必要です。**

### アプリケーション
- ズンドコキヨシ（zundoko.asm）

## 開発環境
- macOS Catalina
- [SB-Assembler](https://www.sbprojects.net/sbasm/) - Macro Cross Assembler
- [minipro](https://gitlab.com/DavidGriffith/minipro.git) - TL866xx controller
