;
; * IPL for SBC6303
; * SB-Assembler
;

        .cr     6301            ; HD6301 Cross Overlay
        .tf     IPL.s19,s19     ; Target File Name
        .lf     IPL             ; List File Name
        .sf     IPL             ; Symbol File Name

; Control Registers = SNAKE_CASE
; Constants = SNAKE_CASE
; Variables = snake_case
; Functions = camelCase

; ********************************************************************
; * HD6303R Internal Registers                                       *
; ********************************************************************
        .in     HD6303R_chip.def

; ********************************************************************
; *  定数 Constants                                                  *
; ********************************************************************
NUL             .eq     $00     ; NUL
BS              .eq     $08     ; Backspace
SPACE           .eq     $20     ; Space
CR              .eq     $0d     ; Carriage Return
LF              .eq     $0a     ; Line Feed
DEL             .eq     $7f     ; Delete

; ----- 開発時メモリマップ -----
; 0000 +--------+
;      |  Work  |
; 0400 |--------|
;      |        |
;      |  User  |
;      |        |
; 1000 |--------|
;      |        |
;      |        |
;      |  Code  |
;      |        |
;      |        |
; 1fff +--------+

RAM_START       .eq     $0020
RAM_END         .eq     $1fff
ROM_START       .eq     $e000
ROM_END         .eq     $ffff
STACK           .eq     $03ff

PROGRAM_START   .eq     $1000   ; プログラム開始アドレス
Rx_BUFFER       .eq     $0100   ; SCI Rx Buffer
Rx_BUFFER_END   .eq     $0148   ; 73byte（72文字分）

; ********************************************************************
; *  変数 Variables                                                  *
; ********************************************************************
        .sm     RAM             ; Select Memory Directive
        .or     $20
check_sum       .bs     1       ; Sレコードチェックサム
byte_count      .bs     1       ; 転送バイト数
load_address    .bs     2       ; 転送アドレス

; ********************************************************************
; *  メインルーチン                                                  *
; ********************************************************************
        .sm     CODE            ; Select Memory Directive
        .or     ROM_START

reset:  sei
; ----- スタックポインタ設定 -----
        lds     #STACK
; ----- PORT設定 -----
        ldab    #$ff            ; 全ポート -> 出力
        stab    <DDR1
; ----- SCI設定 ------
        ldab    #E128|NRZIN     ; 9,600bps
        stab    <RMCR
        ldab    #TE|RE          ; SCI有効化
        stab    <TRCSR
        ldab    <RDR            ; 空読み
        cli

putStartMessage:
        ldx     #MSG_START      ; スタートメッセージ
        jsr     putLine
        bra     loadSRecord:reload

loadSRecord:
.loop   ldab    #'.'
        jsr     putChar
.reload jsr     getChar
        cmpb    #'S'
        bne     :reload         ; 一文字目が"S"でないならもう一度
; ----- レコードタイプの確認 -----
        jsr     getChar
        cmpb    #'9'
        beq     :end            ; レコードタイプ"S9"ならば終了処理
        cmpb    #'1'
        bne     :reload         ; レコードタイプが"S1"でないなら再度読み込み
; ----- バイト数の確認 -----
        bsr     :loadByte
        stab    <check_sum      ; check_sum = byte_count
        subb    #3              ; アドレスとチェックサム分の3バイトを引く
        stab    <byte_count     ; 転送するバイト数
; ----- アドレスの確認 -----
        bsr     :loadByte       ; 上位アドレスを受信
        stab    <load_address
        bsr     :loadByte       ; 下位アドレスを受信
        stab    <load_address+1
        ldx     <load_address
        ldab    <check_sum       ; チェックサムの計算
        addb    <load_address
        addb    <load_address+1
        stab    <check_sum
; ----- データ転送 -----
.data   bsr     :loadByte
        stab    0,x
        addb    <check_sum      ; チェックサムの計算
        stab    <check_sum
        inx
        dec     byte_count
        bne     :data
; ----- チェックサム確認 -----
; チェックサムは各バイト合計の1の補数。チェックサムまで全部足すと$ffになる。
        bsr     :loadByte       ; チェックサムの受信
        addb    <check_sum
        incb                    ; 問題なければ $ff + 1 = $00 となるはず
        beq     :loop
        jsr     putCrlf
        ldx     #ERR_BAD_RECORD
        jsr     putLine
        bra     putStartMessage
; ----- 終了処理 -----
.end    jsr     getChar
        cmpb    #lf             ; "LF"まで受信を続ける
        bne     :end
        ldx     #MSG_LOAD_OK
        jsr     putLine
        jmp     PROGRAM_START  ; PROGRAM_STARTに実行を移す
; ----- 1バイト受信する -----
; 受信する内容は[0-9A-F]でないとならない（チェックしない）
.loadByte
        jsr     getChar
        cmpb    #'A'            ; if B < "A"
        bcs     :1              ; then goto .1
        subb    #7
.1      subb    #$30            ; 数値に変換
        aslb
        aslb
        aslb
        aslb
        tba                     ; Aレジスタに上位4bitをコピー
        jsr     getChar
        cmpb    #'A'            ; if B < "A"
        bcs     :2              ; then goto .2
        subb    #7
.2      subb    #$30            ; 数値に変換
        aba                     ; 上位4bitと下位4bitを加算
        tab                     ; A -> B
        rts

; ------------------------------------------------
; getChar()
; SCIより一文字受信する
;【引数】なし
;【破壊】B
;【結果】B:アスキーコード
; TRCSR = RDRF|ORFE|TDRE|RIE|RE|TIE|TE|WU
;           0    0  = 受信データなし
;           1    0  = 受信データあり
;           0    1  = フレーミングエラー
;           1    1  = オーバーランエラー
; ------------------------------------------------
getChar:
.top    tim     #RDRF|ORFE,<TRCSR      ; SCIステータスレジスタ読み込み
        beq     :top
        tim     #ORFE,<TRCSR
        beq     :end
        ldab    <RDR
        bra     :top
.end    ldab    <RDR
        rts

; ------------------------------------------------
; getLine(X)
; SCIから文字列をRxバッファに受信する（終端記号$00）。エコー、改行付き
;【引数】
;【使用】B, X
;【返値】B:Null, X:バッファ終了位置
; ------------------------------------------------
getLine:
        ldx     #Rx_BUFFER
.loop   bsr     getChar
        cmpb    #BS             ; 入力文字がBSならば…
        beq     :bs             ; バックスペース処理へ
; ----- 行末処理 -----
        cmpb    #CR             ; CR?
        beq     :loop           ; なにもしない。次の文字はLFのはず
        cmpb    #LF             ; LF?
        beq     :end
        cmpb    #DEL            ; b >= DEL ?
        bcc     :loop           ; なにもしない
        cmpb    #SPACE          ; b < SPACE ?
        bcs     :loop           ; なにもしない
        bsr     putChar         ; echo
        stab    0,x             ; バッファに文字を収納
        inx                     ; ポインタを進める
        cpx     #Rx_BUFFER_END  ; バッファ終端チェック
        bne     :loop
        ldab    #BS             ; 終端だったらカーソルとポインタを戻す
        bsr     putChar
        dex
        bra     :loop           ; 次の文字入力
; ----- バックスペース処理 -----
.bs     cpx     #Rx_BUFFER      ; ポインタ位置が一文字目ならば…
        beq     :loop           ; なにもしない
        bsr     putChar         ; 一文字後退
        ldab    #SPACE          ; 空白を表示（文字を消去）
        bsr     putChar
        dex                     ; バッファポインタ-
        ldab    #BS             ; 一文字後退（カーソルを戻す）
        bsr     putChar
        bra     :loop
; ----- 終端処理 -----
; 文字数が72文字の時、次のアドレスを指していない。その場合はポインタを+1する。
.end    cpx     #Rx_BUFFER_END-1
        bne     :noinc
        inx
.noinc  clrb                    ; $00:終端記号
        stab    0,x
        bra     putCrlf         ; putCrlfでrts

; ------------------------------------------------
; putChar(B)
; SCIに一文字送信する
;【引数】B:アスキーコード
;【破壊】なし
;【結果】B:アスキーコード
; ------------------------------------------------
putChar:
.top    tim     #TDRE,<TRCSR
        beq     putChar         ; TDRE=0だったら出力を待つ
        stab    <TDR
        rts

; ------------------------------------------------
; putLine(X)
; SCIに文字列を送信する（終端記号$00）
;【引数】X:文字列の開始アドレス
;【破壊】B, X
;【結果】B:Null, X:文字列の終了アドレス
; ------------------------------------------------
putLine:
.top    ldab    0,x
        beq     :end            ; nullだったら終了
        bsr     putChar
        inx
        bra     :top
.end    rts

; ------------------------------------------------
; putCrlf()
; SCIにCRLFを送信する
;【引数】なし
;【破壊】B
;【結果】B:LF
; ------------------------------------------------
putCrlf:
        ldab    #CR
        bsr     putChar
        ldab    #LF
        bra     putChar         ; putCharでrts

; ------------------------------------------------
; putSpace()
; SCIにSPを送信する
;【引数】なし
;【破壊】B
;【結果】B:SPACE
; ------------------------------------------------
putSpace:
        ldab    #SPACE
        bra     putChar         ; putCharでrts

; ------------------------------------------------
; putWord(D)
; バイナリ数値を16進数字としてSCIに送信する
;【引数】D:数値(Word)
;【破壊】D(A,B)
;【返値】なし
; ------------------------------------------------
putWord:
        pshb
        tab
        bsr     putByte
        pulb
        bra     putByte         ; putByteでrts

; ------------------------------------------------
; putByte(B)
; バイナリ数値を16進数字としてSCIに送信する
;【引数】B:数値(Byte)
;【破壊】B
;【返値】なし
; ------------------------------------------------
putByte:
; ----- 上位4ビットの出力 -----
        pshb
        bsr     highNibbleToStr
        bsr     putChar
; ----- 下位4ビットの出力 -----
        pulb
        bsr     lowNibbleToStr
        bra     putChar         ; putCharでrts

; ------------------------------------------------
; highNibbleToStr
; バイナリ数値の上位4bitを16進文字に変換する
;        High Nibble TO String
;【引数】B:16進値
;【破壊】B
;【返値】B:アスキーコード
; ------------------------------------------------
highNibbleToStr:
        lsrb
        lsrb
        lsrb
        lsrb
        ; continue to lowNibbleToStr
; ------------------------------------------------
; lowNibbleToStr
; バイナリ数値の下位4bitを16進文字に変換する
;【引数】B:16進値
;【破壊】B
;【返値】B:アスキーコード
; ------------------------------------------------
lowNibbleToStr:
        andb    #$0f           ; 上位4bitをマスクする
        addb    #$30           ; 数値をアスキーコードに変換する
        cmpb    #$3a
        bcs     :end
        addb    #$27           ; a-fだったらさらに$27を足して変換する
.end    rts

; ********************************************************************
; *  文字列                                                          *
; ********************************************************************
MSG_START:      .as     "*** Initial program loader for SBC6303 ***",#CR,#LF
                .az     "Please send the S19 file...",#CR,#LF
MSG_LOAD_OK:    .az     " Loading O.K. Jump to $1000.",#CR,#LF
ERR_BAD_RECORD: .az     "Invalid record format.",#CR,#LF,#CR,#LF

; ********************************************************************
; *  Service Routine Jump Table                                      *
; ********************************************************************
        .or     $ffa0
        jmp     reset           ; $ffa0
        jmp     getChar         ; $ffa3
        jmp     getLine         ; $ffa6
        jmp     putChar         ; $ffa9
        jmp     putLine         ; $ffac
        jmp     putCrlf         ; $ffaf
        jmp     putSpace        ; $ffb2
        jmp     putByte         ; $ffb5
        jmp     putWord         ; $ffb8

; ********************************************************************
; *  Interrupt Vectors                                               *
; ********************************************************************
        .or     $ffee
        .dw     0               ; Trap
        .dw     0               ; sci
        .dw     0               ; tof
        .dw     0               ; ocf
        .dw     0               ; icf
        .dw     0               ; irq
        .dw     0               ; swi
        .dw     0               ; nmi
        .dw     reset           ; Reset Vector
